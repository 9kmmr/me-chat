<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       wordpress.org
 * @since      1.0.0
 *
 * @package    Me_Chat
 * @subpackage Me_Chat/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

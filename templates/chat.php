

    <head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->


    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet" >
    <style>
        .container{max-width:1170px; margin:auto;}
        img{ max-width:100%;}
        .inbox_people {
        background: #f8f8f8 none repeat scroll 0 0;
        float: left;
        overflow: hidden;
        width: 40%; border-right:1px solid #c4c4c4;
        }
        .inbox_msg {
        border: 1px solid #c4c4c4;
        clear: both;
        overflow: hidden;
        }
        .top_spac{ margin: 20px 0 0;}


        .recent_heading {float: left; width:40%;}
        .srch_bar {
        display: inline-block;
        text-align: right;
        width: 60%; padding:
        }
        .headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

        .recent_heading h4 {
        color: #05728f;
        font-size: 21px;
        margin: auto;
        }
        .srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
        .srch_bar .input-group-addon button {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        padding: 0;
        color: #707070;
        font-size: 18px;
        }
        .srch_bar .input-group-addon { margin: 0 0 0 -27px;}

        .chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
        .chat_ib h5 span{ font-size:13px; float:right;}
        .chat_ib p{ font-size:14px; color:#989898; margin:auto}
        .chat_img {
        float: left;
        width: 11%;
        }
        .chat_ib {
        float: left;
        padding: 0 0 0 15px;
        width: 88%;
        }

        .chat_people{ overflow:hidden; clear:both;}
        .chat_list {
        border-bottom: 1px solid #c4c4c4;
        margin: 0;
        padding: 18px 16px 10px;
        cursor: pointer;
        }
        .inbox_chat { height: 550px; overflow-y: scroll;}

        .active_chat{ background:#ebebeb;}

        .incoming_msg_img {
        display: inline-block;
        width: 6%;
        }
        .incoming_msg_img img {
            border-radius: 50px;
        }
        .chat-name span {
            font-size: 12px;
            color: #524e4e;
        }
        .received_msg {
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: middle;
        width: 92%;
        }
        .received_withd_msg p {
        background: #ebebeb none repeat scroll 0 0;
        border-radius: 3px;
        color: #000000;
        font-size: 14px;
        margin: 0;
        padding: 5px 10px 5px 12px;
        width: 100%;
        }
        .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 8px 0 0;
        }
        .received_withd_msg { width: 57%;}
        .mesgs {
        float: left;
        padding: 0px 15px 0 25px;
        width: 60%;
        }

        .sent_msg p {
        background: #05728f none repeat scroll 0 0;
        border-radius: 3px;
        font-size: 14px;
        margin: 0; color:#fff;
        padding: 5px 10px 5px 12px;
        width:100%;
        }
        .outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
        .sent_msg {
        float: right;
        width: 46%;
        }
        .input_msg_write input {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        color: #4c4c4c;
        font-size: 15px;
        min-height: 48px;
        width: 100%;
        }

        .type_msg {border-top: 1px solid #c4c4c4;position: relative;}
        .msg_send_btn {
        background: #05728f none repeat scroll 0 0;
        border: 1px solid #05728f;
        
        color: #fff;
        cursor: pointer;
        font-size: 17px;
        
        position: absolute;
        right: 0;
        top: -1px;
       
        }
        .messaging { padding: 0 0 50px 0;}
        .msg_history {
        height: 516px;
        overflow-y: auto;
        }
    </style>
    </head>
<body>
    <div class="container">
        <h3 class=" text-center">Messaging</h3>
        <div class="messaging">
            <div class="inbox_msg">
                <div class="inbox_people">
                <div class="headind_srch">
                    <div class="recent_heading">
                    <h4>Recent</h4>
                    
                    <button type="button" id="add-new-conversation"   class="btn btn-sm btn-icon btn-success"><i class="fa fa-plus" aria-hidden="true"></i></button>
                  

                    </div>
                    <div class="srch_bar">
                    <div class="stylish-input-group">
                        <input type="text" class="search-bar"  placeholder="Search" >
                        <span class="input-group-addon">
                        <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                        </span> </div>
                    </div>
                </div>
                <div class="inbox_chat" id="conversations-chat">
                    
                </div>
                </div>
                <div class="mesgs">
                    <div>
                        
                        <button type="button" id="refresh-message-btn" class="btn btn-info btn-icon pull-right"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                        
                    </div>
                    <div class="msg_history" id="messages-history">

                        <div>
                            <span class="pull-right" id="chat-name"></span>
                        </div>
                        <!-- incoming messages -->
                        
                        <!-- out going messages -->
                      
                        <div id="flag-messages"></div>
                    </div>
                    <div class="type_msg">
                        <div class="input_msg_write">
                            <input type="text" class="write_msg" id="message-text" placeholder="Type a message" />
                            <button class="msg_send_btn" type="button" id="send-btn"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
  
        </div>
    </div>
    <!-- Button trigger modal -->
    
    <!-- Modal -->
    <div class="modal fade" id="new-conversation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">New Conversation</h4>
            <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
           
            <div class="form-group">
                <label for="">Select Friend</label>
                <select class="form-control" name="friend-conversation" id="friend-all">
                   
                </select>
            </div>
           
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="start-conversation">Start Conversation</button>
        </div>
        </div>
    </div>
    </div>
    <input type="hidden" value="" id="active-chat-person">
    <input type="hidden" value="" id="active-chat-conversation">
</body>

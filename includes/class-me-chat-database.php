<?php

class Me_Chat_Database {
    protected $plugin_name;	
    protected $version;
    private $me_chat_version = 1.01;


    /**
     * install db
     */
    public function me_chat_db_install() {
        global $wpdb;       

        $conversations_table = $wpdb->prefix . 'MC_conversations';
        $messages_table = $wpdb->prefix . 'MC_messages';
        $file_conversations_table = $wpdb->prefix . 'MC_file_conversations';

        $charset_collate = $wpdb->get_charset_collate();

        $sql_conversations = "CREATE TABLE $conversations_table (
            ID  INT NOT NULL AUTO_INCREMENT,
            ownerID  INT NOT NULL ,
            friendID INT  NOT NULL,            
            Datecreate TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,          
            PRIMARY KEY  (ID)
            ) $charset_collate;      
        ";
        $sql_messages = "CREATE TABLE $messages_table (
            ID  INT NOT NULL AUTO_INCREMENT,
            cid  INT NOT NULL ,
            from_uid INT  NOT NULL, 
            to_uid INT  NOT NULL,
            content TEXT  NOT NULL,           
            Datecreate TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,          
            PRIMARY KEY  (ID)
            ) $charset_collate;      
        ";
        $sql_file_conversations = "CREATE TABLE $file_conversations_table (
            ID  INT NOT NULL AUTO_INCREMENT,
            cid  INT NOT NULL ,
            filename TEXT  NOT NULL,                     
            Datecreate TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,          
            PRIMARY KEY  (ID)
            ) $charset_collate;      
        ";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        try {
            dbDelta( $sql_conversations );
            dbDelta( $sql_messages );
            dbDelta( $sql_file_conversations );
        }catch (Exception $e) {
            var_dump($e->getMessages());
        }
            
        add_option( 'me_chat_db_version', $this->me_chat_version );
    }
    /**
     * update db
     */
    public function me_chat_update_db_check() {        
        if ( get_site_option( 'me_chat_db_version' ) != $this->me_chat_version ) {
            me_chat_db_install();
        }
        
    }
    /**
     * uninstall
     */

    public function me_chat_db_uninstall() {
        global $wpdb;      

        $conversations_table = $wpdb->prefix . 'MC_conversations';
        $messages_table = $wpdb->prefix . 'MC_messages';
        $file_conversations_table = $wpdb->prefix . 'MC_file_conversations';

        $charset_collate = $wpdb->get_charset_collate();

        $sql_conversations_table = "DROP TABLE IF EXISTS ".$conversations_table ." ; " ;
        $sql_messages_table = "DROP TABLE IF EXISTS ".$messages_table ." ; " ;   
        $sql_file_conversations_table = "DROP TABLE IF EXISTS ".$file_conversations_table ." ; " ; 

        try {
            $wpdb->query( $sql_conversations_table );     
            $wpdb->query( $sql_messages_table );     
            $wpdb->query( $sql_file_conversations_table );     
            
        } catch(Exception $e) {
            var_dump($e->getMessages());
        }

        delete_option('me_chat_db_version');
    }
}

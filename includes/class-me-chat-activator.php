<?php

/**
 * Fired during plugin activation
 *
 * @link       wordpress.org
 * @since      1.0.0
 *
 * @package    Me_Chat
 * @subpackage Me_Chat/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Me_Chat
 * @subpackage Me_Chat/includes
 * @author     lamvu <tunglam195@gmail.com>
 */
class Me_Chat_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-me-chat-database.php';
		$Me_Chat_Database = new Me_Chat_Database();
		$Me_Chat_Database->me_chat_db_install();
		flush_rewrite_rules();
	}

}

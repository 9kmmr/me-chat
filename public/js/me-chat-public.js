(function( $ ) {
	'use strict';
	$(function() {
		var friend = _.template('<option value={"id":<%- ID %>,"avatar":"<%- avatar %>","name":"<%- nicename %>"} ><%- nicename %></option>');
		var history_conversations = _.template('<div class="chat_list"  data-cid="<%- ID %>" data-friendid="<%- friendid %>">'
			+'<div class="chat_people" >'
				+'<div class="chat_img"> <img src="<%- avatar %>" alt="<%- name %>"  style="border-radius:50px;"> </div>'
				+'<div class="chat_ib">'
					+'<h5><%- name %> <span class="chat_date"><%- datecreated %></span></h5>'
					
				+'</div>'
			+'</div>'
		+'</div> ');
		var new_active_conversations = _.template('<div class="chat_list active_chat"  data-cid="<%- ID %>" data-friendid="<%- friendid %>">'
			+'<div class="chat_people" >'
				+'<div class="chat_img"> <img src="<%- avatar %>" alt="<%- name %>"  style="border-radius:50px;"> </div>'
				+'<div class="chat_ib">'
					+'<h5><%- name %> <span class="chat_date"><%- datecreated %></span></h5>'
					
				+'</div>'
			+'</div>'
		+'</div> ');

		var my_message = _.template('<div class="outgoing_msg"><div class="sent_msg"><p><%- message %></p><span class="time_date"><%- datecreated %></span></div></div>');

		var friend_message = _.template('<div class="incoming_msg">'
			+'<div class="incoming_msg_img"> <img src="<%- avatar %>" alt="<%- name %>"> </div>'
			+'<div class="received_msg">'
				+'<span class="chat-name">'
					+'<span><%- name %></span>'
				+'</span>'
				+'<div class="received_withd_msg">'
					+'<p><%- message %></p>'
					+'<span class="time_date"> <%- datecreated %></span>'
				+'</div>'
			+'</div>'
		+'</div>')




/**
 * SEND MESSAGE
 */

		$('#send-btn').click(function(){
			if ($('#message-text').val().trim())
			sendMessage()
		})

		$('#message-text').keypress(function(e){
			if (e.keyCode==13) {
				if ($('#message-text').val().trim())
				sendMessage()
			}
		})

		$('#refresh-message-btn').click(function(){
			getmyMessages($('#active-chat-conversation').val(),0);
		})


/**
 * new conversation
 */
		$('#start-conversation').click(function(){
			if ($('#friend-all').val()) {
				console.log($('#friend-all').val())
				const datafriend = JSON.parse($('#friend-all').val());
				$(".chat_list").removeClass('active_chat');

				getExistConversationWithFriend(datafriend.id, function(res){
					$("#active-chat-person").val(datafriend.id);
					$('#chat-name').text(datafriend.name);
					if (res) {

					}else {
						$("#active-chat-conversation").val('')
						$('#messages-history').html('<div><span class="pull-right" id="chat-name"></span></div><div id="flag-messages"></div>')		
						$('#conversations-chat').prepend(new_active_conversations({ID:null, friendid:datafriend.id,avatar: datafriend.avatar,name: datafriend.name,datecreated: (new Date().getMonth()+1 )+'-'+new Date().getDate() }));
					}
					$('#new-conversation').modal('hide');
				})


				
				
			} else {

			}
		})

		function setActiveConversation(cid) {

		}

/**
 * ADD NEW CONVERSATION
 */
		$('#add-new-conversation').click(function() {
			$.ajax({
				cache: false,
				type: "GET",
				url: wp_ajax.ajax_url,
				data: {'action': 'get_all_users'},
				success: function( response ){					

					var users = JSON.parse(response);
					$('#friend-all').empty();
					
					users.users.forEach(element => {
						
						$('#friend-all').append(friend({ID: element.ID,avatar: element.meta.avatar,nicename: element.meta.nicename}))
					});
					$('#new-conversation').modal('show');
				},
				error: function( xhr, status, error ) {
					console.log( 'Status: ' + xhr.status );
					console.log( 'Error: ' + xhr.responseText );
				}
			});
		})

/**
 *  GET ALL MY CONVERSATION
 */
		if ($("#conversations-chat")) {
			getMyConversations();
		}
		function getMyConversations() {
			
			$.ajax({
				cache: false,
				type: "GET",
				url: wp_ajax.ajax_url,
				data: {'action': 'get_my_conversations'},
				success: function( response ) {					
					
					if (response) {
						var data_history_pp_chat = JSON.parse(response);
						console.log(data_history_pp_chat)
						if ($("#conversations-chat")&&data_history_pp_chat['success']) {
							
							var counter=0;
							data_history_pp_chat['conversations'].forEach((element ,index)=> {								
								counter++;
								if (index==0) {
									$('#conversations-chat').append(new_active_conversations({ID:element.ID, friendid: (element.friendID==cuid)?element.ownerID:element.friendID,avatar: element.avatar,name: element.name,datecreated: element.Datecreate }));
								} else
									$("#conversations-chat").append(history_conversations({ID:element.ID, friendid:(element.friendID==cuid)?element.ownerID:element.friendID,avatar: element.avatar,name: element.name,datecreated: element.Datecreate }));
								if (counter==data_history_pp_chat['conversations'].length) {
									
									$(".chat_list").click(function(){
												var cid = $(this).data('cid');
												$("#active-chat-person").val($(this).data('friendid'))
												
												$(".chat_list").removeClass('active_chat');
												$(this).addClass('active_chat');
												getmyMessages(cid, 0);
									})
									$("#active-chat-person").val($(".chat_list").first().data('friendid'))
												
									getmyMessages(data_history_pp_chat['conversations'][0].ID,0);
								}
							});
						}
					}
				},
				error: function( xhr, status, error ) {
					console.log( 'Status: ' + xhr.status );
					console.log( 'Error: ' + xhr.responseText );
				}
			});
		}
		function getmyMessages(cid, limit) {
			$('#active-chat-conversation').val(cid);
			$('#messages-history').html('<div><span class="pull-right" id="chat-name"></span></div><div id="flag-messages"></div>')
			$.ajax({
				cache: false,
				type: "GET",
				url: wp_ajax.ajax_url,
				data: {'action': 'get_my_messages', 'cid' : cid, 'limit': limit | 0 },
				success: function( response ){	
					console.log(response)
					var data_messages = JSON.parse(response);
					if (data_messages['success']) {
						if(data_messages['messages'].length) {
							data_messages['messages'].forEach((element,index) => {
								if (element.from_uid==cuid) {
									$(my_message({message: element.content, datecreated: element.Datecreate})).insertBefore('#flag-messages');
								} else {
									$(friend_message({avatar: element.avatar, name: element.name, message: element.content,datecreated: element.Datecreate })).insertBefore('#flag-messages');
								}
							})
						}
					}
					//().insertBefore('#flag-messages');
				},
				error: function( xhr, status, error ) {
					console.log( 'Status: ' + xhr.status );
					console.log( 'Error: ' + xhr.responseText );
				}
			});
		}
		function getExistConversationWithFriend(friendid, callback) {
			$.ajax({
				cache: false,
				type: "GET",
				url: wp_ajax.ajax_url,
				data: {'action': 'get_friend_message', 'friendid' : friendid },
				success: function( response ){	
					console.log(response)
					var data_conversation = JSON.parse(response);
					if (data_conversation['success']) {
						callback(data_conversation['ID']);
					}
				}
			})
		}

/**
 * SEND MESSAGE
 */
		function sendMessage() {
			if ($("#active-chat-person").val().trim() || !$('#active-chat-conversation').val()) {
				newConversation(parseInt($("#active-chat-person").val().trim()), function(res){
					if (res) {
						const data_return = JSON.parse(res);
						console.log(data_return)
						$('#active-chat-conversation').val(data_return['cid'])
						if (data_return['success'])
						sendMessageNow(data_return['cid'], $("#active-chat-person").val().trim(),$('#message-text').val().trim() );
					}
				})
			} else if ($('#active-chat-conversation').val() && $("#active-chat-person").val()) {
				sendMessageNow($('#active-chat-conversation').val().trim(), $("#active-chat-person").val().trim(), $('#message-text').val().trim());
				
			}
		}
		function sendMessageNow(cid, to_uid, message){
			console.log(cid, to_uid)
			console.log(message)
			$('#message-text').val('')
			$(my_message({message: message, datecreated: getDateCreated()})).insertBefore('#flag-messages');
			var dataJSON = {
				'action'	: 'new_messages',
				'cid'		: parseInt(cid),
				'to_uid' 	: parseInt(to_uid),
				'message' 	: message,
				'new_message_nonce' : new_message_nonce
			};
			
			$.ajax({
				cache: false,
				type: "POST",
				url: wp_ajax.ajax_url,
				data: dataJSON,
				success: function( response ){					
					console.log(response);	
								
				},
				error: function( xhr, status, error ) {
					console.log( 'Status: ' + xhr.status );
					console.log( 'Error: ' + xhr.responseText );
				}
			});
		}

/**
 * AJAX NEW CONVERSATION
 * @param {*} friendid 
 * @param {*} callback 
 */
		function newConversation(friendid, callback) {
			var dataJSON = {
				'action'	: 'new_conversation',
				'friend_id'		: friendid	,
				'new_conversation_nonce': new_conversation_nonce			
			};
			
			$.ajax({
				cache: false,
				type: "POST",
				url: wp_ajax.ajax_url,
				data: dataJSON,
				success: function( response ){	
					console.log(response)				
					if (response) callback(response)
				},
				error: function( xhr, status, error ) {
					console.log( 'Status: ' + xhr.status );
					console.log( 'Error: ' + xhr.responseText );
				}
			});
		}
		
		function getDateCreated() {
			const today = new Date();
			let dd = today.getDate();
			let mm = today.getMonth() + 1; // January is 0!
			const yyyy = today.getFullYear();
			let h = today.getHours();
			let m = today.getMinutes();
			let s = today.getSeconds();
			if (dd < 10) {
				dd = '0' + dd;
			}
			if (mm < 10) {
				mm = '0' + mm;
			}
			if (h < 10) {
				h = '0' + h;
			}
			if (m < 10) {
				m = '0' + m;
			}
			if (s < 10) {
				s = '0' + s;
			}
			// ex : today is 2018-07-30 00:00:00
			return yyyy + '-' + mm + '-' + dd + ' ' + h + ':' + m + ':' + s;
		}
		

	});



})( jQuery );
